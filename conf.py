import sys
import os
import re
sys.path.insert(0, os.path.abspath('NDN_TR'))

extensions = ['customizations', 'sphinxcontrib.bibtex']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document.
master_doc = 'trace'

# General information about the project.
project = u'Traceroute for Named Data Networking'
version = u''

copyright = u''
author = u'Susmit Shannigrahi, Dan Massey, Christos Papadopoulos'

NDN = {
    'tr-number': 'NDN-0055',
    'revisions': [
        {
            'number': '2',
            'date': 'March 24, 2017',
            'link': '',
        },
        {
            'number': '1',
            'date': 'January 22, 2012',
            'link': 'https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/ccnx-trace/trace_writeup.pdf',
        },
    ]}

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This patterns also effect to html_static_path and html_extra_path
exclude_patterns = []

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# -- Options for HTML output ----------------------------------------------
html_theme = 'alabaster'
# html_theme_options = {}
html_static_path = ['_static']

# -- Options for LaTeX output ---------------------------------------------

offset = -85
NDN_revision_text = ''
for revision in NDN['revisions']:
    NDN_revision_text += '''%
\put(\strip@pt\@tempdimb,\strip@pt\@tempdimc){%
    \makebox(0,''' + str(offset) + ''')[l]{\color{black}%
Revision ''' + revision['number'] + ''': ''' + revision['date']
    if revision['link'] and revision['link'] != '':
        NDN_revision_text += ' \\url{%s}' % revision['link']
    NDN_revision_text += '''}
  }%'''
    offset -= 20

latex_elements = {
    'papersize': 'letterpaper',
    'pointsize': '10pt',
    'tableofcontents': '',
    'geometry': '\\usepackage[margin=0.8in]{geometry}',
    'preamble': '''
\usepackage{eso-pic,xcolor}
\makeatletter
\AddToShipoutPicture*{%
\setlength{\@tempdimb}{20pt}%
\setlength{\@tempdimc}{\paperheight}%
\setlength{\unitlength}{1pt}%
\put(\strip@pt\@tempdimb,\strip@pt\@tempdimc){%
    \makebox(0,-60)[l]{\color{blue}%
NDN, Technical Report ''' + NDN['tr-number'] + '''. \url{http://named-data.net/techreports.html}}
  }%
''' + NDN_revision_text + '''
}
\makeatother

\\usepackage{etoolbox}
\\usepackage{xstring}
\\DeclareListParser{\\doslashlist}{/}
\\newcounter{ndnNameComponentCounter}%
\\newcommand{\\ndnName}[1]{{%
  \\setcounter{ndnNameComponentCounter}{0}%
  \\renewcommand{\\do}[1]{{%
    \\ifnumgreater{\\value{ndnNameComponentCounter}}{0}{\\allowbreak/}{}%
    \\ifnumodd{\\value{ndnNameComponentCounter}}{}{}%
    \\detokenize{##1}}%
    \\stepcounter{ndnNameComponentCounter}}%
``{\\fontfamily{cmtt}\\small\\selectfont\\IfBeginWith{#1}{/}{/}{}\\doslashlist{#1}}''%
}}

\\sphinxsetup{TitleColor={rgb}{0,0,0}}
\\pagestyle{headings}
\\fvset{frame=none,fontfamily=cmtt}
\\renewcommand*{\\ttdefault}{cmtt}
''',

    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

latex_toplevel_sectioning = 'section'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, '%s-%s.tex' % (NDN['tr-number'].lower(), master_doc), project, author, '../../NDN_TR/ndn-tr'),
]
